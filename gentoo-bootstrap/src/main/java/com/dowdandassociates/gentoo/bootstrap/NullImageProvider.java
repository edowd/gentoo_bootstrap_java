/*
 *
 * NullImageProvider.java
 *
 *-----------------------------------------------------------------------------
 * Copyright 2019 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-----------------------------------------------------------------------------
 *
 */

package com.dowdandassociates.gentoo.bootstrap;

import com.amazonaws.services.ec2.model.Image;

import com.google.common.base.Optional;

import com.google.inject.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NullImageProvider implements Provider<Optional<Image>> 
{
    private static Logger log = LoggerFactory.getLogger(NullImageProvider.class);

    public NullImageProvider()
    {
    }

    @Override
    public Optional<Image> get()
    {
        Optional<Image> image = Optional.absent();
        return image;
    }

}

