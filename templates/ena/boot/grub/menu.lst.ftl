default 0
timeout 3
title EC2
root (hd0,0)
<#if useRootLabel?? && useRootLabel && rootfstype?starts_with("ext")>
kernel /boot/kernel root=LABEL=root rootfstype=${rootfstype} net.ifnames=0
initrd /boot/initramfs
<#else>
kernel /boot/bzImage root=/dev/xvda1 rootfstype=${rootfstype}
</#if>

