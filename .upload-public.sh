#!/bin/bash

# Upload templates

aws s3 sync \
--acl public-read \
--content-type text/plain \
templates/ s3://public.dowdandassociates.com/products/gentoo_bootstrap_java/raw/master/templates/

# Upload config

aws s3 sync \
--acl public-read \
--content-type text/plain \
config/ s3://public.dowdandassociates.com/products/gentoo_bootstrap_java/raw/master/config/


