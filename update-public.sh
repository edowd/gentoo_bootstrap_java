#!/bin/bash

aws s3 sync \
templates \
s3://public.dowdandassociates.com/products/gentoo_bootstrap_java/raw/master/templates/ \
--acl public-read

aws s3 sync \
config \
s3://public.dowdandassociates.com/products/gentoo_bootstrap_java/raw/master/config/ \
--acl public-read


